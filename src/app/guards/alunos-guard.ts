import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateChild, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../login/auth.service';

@Injectable()
export class AlunosGuard implements CanActivateChild {

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {

      console.log('AlunosGuard: Guarda de rota filha')

    if (state.url.includes('editar')) {
      //alert('Usuário sem acesso!');
      //return false;
    }

    return true;
  }

  constructor(private authService: AuthService, private router: Router) { }

}
