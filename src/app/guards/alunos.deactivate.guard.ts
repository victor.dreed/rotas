import { IFormCanDeactivate } from './iform-candeactivate';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanDeactivate } from '@angular/router';

import { Observable } from 'rxjs/Rx';
import { AlunoFormularioComponent } from './../alunos/aluno-formulario/aluno-formulario.component';

@Injectable()
export class AlunosDeactivateGuard implements CanDeactivate<IFormCanDeactivate> {
    canDeactivate(
      component: IFormCanDeactivate,
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
    ): Observable<boolean>|Promise<boolean>|boolean {

      console.log('guarda do hue');

      // return component.podeMudarRota();
      return component.podeDesativar();
  }
}
